
public class fac {

	public static void main(String[] args) {

		System.out.println(fak(5));

		System.out.println(fakrec(5));
	}

	public static int fak(int k) {
		int j = 1;
		for (int i = 1; i <= k; i++) {
			j *= i;
		}
		return j;
	}

	public static int fakrec(int k) {
		if (k == 1) {
			return 1;
		} else {
			return k * fakrec(k - 1);
		}

	}
	
	public static int fakEndRec(int k) {
		if (k == 1) {
			return 1;
		} else {
			return k * fakEndRec(k - 1);
		}

	}
}
