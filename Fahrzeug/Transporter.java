
public class Transporter extends Fahrzeug{
	
	private int sitzplatz;

	public Transporter(int baujahr, String hersteller, String farbe, String typ, int sitzplatz) {
		super(baujahr, hersteller, farbe, typ);
		this.sitzplatz = sitzplatz;
	}

	public int getSitzplatz() {
		return sitzplatz;
	}

	public void setSitzplatz(int sitzplatz) {
		this.sitzplatz = sitzplatz;
	}
	
	public String toString(){
		return "[" + super.getBaujahr() + "," + super.getHersteller() + "," + super.getFarbe() + "," + sitzplatz + "]";

	}
	


}
