
public class PKW extends Fahrzeug{

	private String besonderheit;

	public PKW(int baujahr, String hersteller, String farbe, String typ, String besonderheit) {
		super(baujahr, hersteller, farbe, typ);
		this.besonderheit = besonderheit;
	}

	public String getBesonderheit() {
		return besonderheit;
	}

	public void setBesonderheit(String besonderheit) {
		this.besonderheit = besonderheit;
	}

	public String toString(){
		return "[" + super.getBaujahr() + "," + super.getHersteller() + "," + super.getFarbe() + "," + besonderheit + "]";
	}
}
