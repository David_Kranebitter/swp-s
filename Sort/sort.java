import java.util.ArrayList;

public class sort {

	public static void main(String[] args) {
		int[] arr = { 5, 1, 87, 2, 6, 9, 4, 24 };

		ArrayList<Integer> arr2 = new ArrayList<Integer>();
		/*
		 * bubblesort(arr);
		 * 
		 * System.out.print("["); for (int element : arr) {
		 * System.out.print(element + ", "); } System.out.print("]");
		 * 
		 * System.out.println(); System.out.println();
		 */
		arr2 = insertSort(arr);

		System.out.print("[");
		for (int element : arr2) {
			System.out.print(element + ", ");
		}
		System.out.print("]");
	}

	static int[] bubblesort(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			for (int j = 0; j < arr.length - i; j++) {
				if (arr[j] > arr[j + 1]) {
					swap(arr, j, j + 1);
				}
			}
		}
		return arr;
	}

	static int[] swap(int[] arr, int pos1, int pos2) {
		int help = arr[pos1];
		arr[pos1] = arr[pos2];
		arr[pos2] = help;

		return arr;
	}

	static ArrayList<Integer> insertSort(int[] arr) {
		ArrayList<Integer> resArr = new ArrayList<Integer>();
		int index = 0;

		resArr.add(arr[0]);

		for (int i = 1; i < arr.length; i++) {
			for (int element : resArr) {

				if (arr[i] > element) {
				index++;
				}
				resArr.add(index, arr[i]);
				index = 0;
			}
		}
		return resArr;
	}

}
