
public class Main {

	public static void main(String[] args) {
		/*
		 * Node n1 = new Node(3); Node n2 = new Node(7); Node n3 = new Node(4);
		 * 
		 * n1.setNext(n2); n2.setNext(n3); Node n = n1;
		 * 
		 * while(n.hasNext()){ System.out.println(n.getValue()); n = n.next(); }
		 * 
		 * System.out.println(n.getValue());
		 */

		MyLinkedList ll = new MyLinkedList();
		ll.add(3);
		ll.add(5);
		ll.add(8);
		ll.print();

		System.out.println("-----Nodes------");

		Node n;
		Node n2 = new Node(3);
		n = n2;
		n.setValue(5);
		System.out.println(n.getValue());
		System.out.println(n2.getValue());

		System.out.println("----Primitives----");

		int i;
		int i2 = 3;
		i = i2;
		i = 5;
		System.out.println(i);
		System.out.println(i2);

		System.out.println("-----String------");

		String s;
		String s2 = new String("servas");
		String s3 = new String("servas");

		if (s2 == s3) {
			System.out.println("gleich");
		} else {
			System.out.println("nicht gleich");
		}
		s = s2;
		s = "ha";
		System.out.println(s);
		System.out.println(s2);

	}
}
