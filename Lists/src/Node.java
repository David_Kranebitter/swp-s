
public class Node {
	Node next;
	Node previous;
	int value;

	public Node(int val) {
		this.value = val;
		next = null;
	}

	public void setNext(Node n) {
		this.next = n;
	}

	public void setPrevious(Node n) {
		this.previous = n;
	}

	public Node next() {
		return next;
	}

	public Node previous() {
		return previous;
	}

	public boolean hasNext() {
		if (this.next == null) {
			return false;
		}
		return true;
	}

	public boolean hasPrevious() {
		if (this.previous == null) {
			return false;
		}
		return true;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
