
public class MyLinkedList {
	Node head;
	Node tail;
	int size;

	public MyLinkedList() {
		size = 0;
		head = null;
	}

	public void add(int i) {
		Node n = new Node(i);
		size++;
		if (head == null) {
			head = n;
		} else {
			Node nlauf;
			nlauf = head;
			while (nlauf.hasNext()) {
				nlauf = nlauf.next();
			}
			nlauf.setNext(n);
			n.setPrevious(nlauf);
			tail = nlauf;
		}

	}

	public boolean isEmpty() {
		return size == 0;
	}
	
	public void print(){
		Node nlauf = head;
		while(nlauf.hasNext()){
			System.out.println(nlauf.getValue());
			nlauf = nlauf.next;
		}
		System.out.println(nlauf.getValue());
	}

	public void iterateBackward() {
		Node nlauf = tail;
		while (nlauf.hasPrevious()) {
			System.out.println(nlauf.getValue());
			nlauf = nlauf.previous;
		}
		System.out.println(nlauf.getValue());
	}

	public void iterateForward() {
		Node nlauf = head;
		while (nlauf.hasNext()) {
			System.out.println(nlauf.getValue());
			nlauf = nlauf.next;
		}
		System.out.println(nlauf.getValue());
	}
}
